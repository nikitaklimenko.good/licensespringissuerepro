﻿
# License spring issue reproduce

We've ran into an issue when using LicenseSpring `GetCustomerLicenseUsers` API. See `Program.cs` for reproduce.


## What happens

When calling `GetCustomerLicenseUsers` for an email that is not registered in license spring, exception is raised:

```
UnknownLicenseSpringException: Unknown error encountered.
Status code BadRequest, error code 400.
```

## What is expected

API should return empty array of customers instead of raising an exception if user is not yet registered within LicenseSpring.
