﻿using LicenseSpring;

// Fill in with your project information
const string ApiKey = "";
const string SharedKey = "";
const string ProductCode = "";

var m = LicenseManager.GetInstance();
m.Initialize(CreateConfiguration(), new LicenseFileStorageEx());
m.ClearLocalStorage();
try {
    // Important: Issue only happens when there's no such customer
    var customer = new Customer("gregor@hotreload.com");
    var users = m.GetCustomerLicenseUsers(customer);
    Console.WriteLine("users foudn: " + users.Length);
} catch (Exception ex) {
    // UnknownLicenseSpringException: Unknown error encountered.
    // Status code BadRequest, error code 400.
    Console.WriteLine(ex.GetType().Name + ": " + ex.Message);
}

static Configuration CreateConfiguration() {
    return new Configuration(
        apiKey: ApiKey,
        sharedKey: SharedKey,
        productCode: ProductCode,
        appName: System.Reflection.Assembly.GetExecutingAssembly().GetName().Name,
        appVersion: System.Reflection.Assembly.GetExecutingAssembly().GetName().Version?.ToString(),
        extendedOptions: new ExtendedOptions {
            LicenseFilePath = null,
            HardwareID = "",
            CollectNetworkInfo = false,
            DeviceIdAlgorithm = DeviceIDAlgorithm.Gen2,
            EnableVMDetection = true,
        }
    );
}
